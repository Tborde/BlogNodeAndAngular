var mongoose = require('mongoose');
var user = mongoose.model('User');

exports.allUser = function(req, res) {
    user.find(function(err, users) {
        if (err) {
            res.send(err);
        }
        res.send(users);
    });
};

exports.addUser = function(req, res) {
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.password = req.body.password;
    user.age = req.body.age;
    user.email = req.body.email;
    user.save(function(err) {
        if (err) {
            res.send(err);
        }
        res.send({ confirmMessage: 'user added' });
    });
};

exports.oneUser = function(req, res) {
    user.findById(req.params.id, function(err, user) {
        if (err) {
            res.send(err);
        }
        res.send(user);
    });
};

exports.updateUser = function(req, res) {
    user.findById(req.params.id, function(err, user) {
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.password = req.body.password;
        user.age = req.body.age;
        user.email = req.body.email;
        user.save(function(err) {
            if (err) {
                res.send(err);
            }
            res.send({ confirmMessage: 'user updated' });
        });
    });
};

exports.deleteUser = function(req, res) {
    user.findById(req.params.id, function(err, user) {
        user.remove(function(err) {
            if (err) {
                res.send(err);
            }
            res.send({ confirmMessage: 'user removed' });
        });
    });
};
