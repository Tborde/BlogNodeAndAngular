module.exports = function(app) {
    var user = require('../controllers/userController');

    app.route('/').get(user.allUser);

    app.route('/add').post(user.addUser);

    app.route('/:id').get(user.oneUser);

    app.route('/:id/update').put(user.updateUser);

    app.route('/:id/delete').delete(user.deleteUser);
};
