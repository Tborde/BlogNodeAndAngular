var mongoose = require('mongoose');

var userShema = mongoose.Schema({
    firstName: String,
    lastName: String,
    password: String,
    age: Number,
    email: String,
});

module.exports = mongoose.model('User', userShema);
