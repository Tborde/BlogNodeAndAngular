var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    User = require('./models/userModel');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/myBlog');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var userRoutes = require('./routes/userRoute');
userRoutes(app);

app.listen(port, function() {
    console.log('Listening on port ' + port);
});
